// Le fichier Serveur.h récupère les messages cbor envoyés sur le port 6789,
// et modifie le fichier svg en fonction des informations récupérées.

#ifndef SERVEUR_H
#define SERVEUR_H

// --includes--
#include <cbor.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <librsvg/rsvg.h>
#include <gio/gfile.h>
#include <tinyxml2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <thread>
#include <iostream>
#include <chrono>
#include <bits/stdc++.h>

// --packages--
using namespace tinyxml2;
using namespace std;

// --déclarations--
class Serveur{

  // --variables--
private:
  bool lancer = false;
  int server,client;
  char buffer[1024];
  string str_buffer;
  struct sockaddr_in server_addr = { 0 };
  XMLDocument svg_data;
  XMLPrinter printer;
  XMLElement *terre;
  XMLElement *tank1;
  XMLElement *tank2;
  XMLElement *roquette;
  XMLElement *armement;
  XMLElement *obus;
  XMLElement *pretRoquette;
  XMLElement *pretObus;
  XMLElement *rechargeRoquette;
  XMLElement *rechargeObus;
  XMLElement *ennemi;
  vector <string> tokens;
  string intermediate;

  // --fonctions--
public:
  Serveur();
  void start(GtkWidget*widget);
  void decrypteCbor(GtkWidget *widget);
  void lancement();
};
#endif

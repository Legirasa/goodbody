// Le fichier Client.cpp créé les chaînes d'informations qui seront ensuite
// envoyées en UDP au format cbor sur le port 6789. Par ailleurs, ce fichier gère
// aussi les déplacements de tous les élements du fichier svg.

// --includes--
#include "Client.h"

// --constructeur--
Client::Client(){

}
// -- fonctions--

// Nom de fonction : "bouclePrincipale"
// Paramètres :
// Return : void
// Description : Cette fonction créé les paquets cbor en fonction des positions
// de tous les éléments présents dans le svg, elle appelle les fonctions de déplacement
// des objets, et enfin, envoie ce paquet sur le port 6789 en UDP
void Client::bouclePrincipale(){

  // Création du client et de son adresse
    sin.sin_family = AF_INET;
    sin.sin_port = htons(6789);
    sin.sin_addr.s_addr = inet_addr("127.0.0.1");
    int client = socket(AF_INET, SOCK_DGRAM, 0);
    if( client < 0){
        printf("error client\n");
        exit(1);
    }
    printf("client created\n");
    int to = sizeof(sin);

    // Boucle principale
    while(true){
      if(lancer){

        // Thread pour réduire le délai de la boucle
        this_thread::sleep_for(chrono::milliseconds(100));

        // Gestion des ennemis
        if(countdownEnnemi == 0 && !ennemiEnDeplacement){
          typeOfEnnemi = creerEnnemi();
        }
        if(ennemiEnDeplacement){
          deplacementEnnemi(typeOfEnnemi);
        }
        else{
          countdownEnnemi--;
        }

        // Déplacements
        deplacementTerre(); // Déplacement de la terre
        deplacementTankJoueur(); // Déplacement du tank joueur
        if(tirObusEnCours){ // Déplacement de l'obus
          deplacementObus();
          deplacementObus();
          deplacementObus();
        }
        if(tirRoquetteEnCours){ // Déplacement de la roquette
          deplacementRoquette();
          deplacementRoquette();
        }

        //Création du paquet cbor
        cbor_item_t *cbor_root = cbor_new_definite_map(21);
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement de la terre
                .key = cbor_move(cbor_build_string("|rotationTerre|")),
                .value = cbor_move(cbor_build_string(rotationTerreEntiere.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du tank joueur
                .key = cbor_move(cbor_build_string("|tank1joueurX|")),
                .value = cbor_move(cbor_build_string(positionTank1X.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du tank joueur
                .key = cbor_move(cbor_build_string("|tank1joueurY|")),
                .value = cbor_move(cbor_build_string(positionTank1Y.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du tank joueur
                .key = cbor_move(cbor_build_string("|tank2joueurX|")),
                .value = cbor_move(cbor_build_string(positionTank2X.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du tank joueur
                .key = cbor_move(cbor_build_string("|tank2joueurY|")),
                .value = cbor_move(cbor_build_string(positionTank2Y.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement de l'obus
                .key = cbor_move(cbor_build_string("|obusX|")),
                .value = cbor_move(cbor_build_string(positionObusX.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement de l'obus
                .key = cbor_move(cbor_build_string("|obusY|")),
                .value = cbor_move(cbor_build_string(positionObusY.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement de la roquette
                .key = cbor_move(cbor_build_string("|roquetteX|")),
                .value = cbor_move(cbor_build_string(positionRoquetteX.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement de la roquette
                .key = cbor_move(cbor_build_string("|roquetteY|")),
                .value = cbor_move(cbor_build_string(positionRoquetteY.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement de l'armement
                .key = cbor_move(cbor_build_string("|armementX|")),
                .value = cbor_move(cbor_build_string(positionArmementX.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement de l'armement
                .key = cbor_move(cbor_build_string("|armementY|")),
                .value = cbor_move(cbor_build_string(positionArmementY.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du message "PRÊT !" de l'obus
                .key = cbor_move(cbor_build_string("|pretObusX|")),
                .value = cbor_move(cbor_build_string(positionPretObusX.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du message "PRÊT !" de l'obus
                .key = cbor_move(cbor_build_string("|pretObusY|")),
                .value = cbor_move(cbor_build_string(positionPretObusY.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du message "PRÊT !" de la roquette
                .key = cbor_move(cbor_build_string("|pretRoquetteX|")),
                .value = cbor_move(cbor_build_string(positionPretRoquetteX.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du message "PRÊT !" de la roquette
                .key = cbor_move(cbor_build_string("|pretRoquetteY|")),
                .value = cbor_move(cbor_build_string(positionPretRoquetteY.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du message "RECHARGEMENT..." de l'obus
                .key = cbor_move(cbor_build_string("|rechargementObusX|")),
                .value = cbor_move(cbor_build_string(positionRechargementObusX.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du message "RECHARGEMENT..." de l'obus
                .key = cbor_move(cbor_build_string("|rechargementObusY|")),
                .value = cbor_move(cbor_build_string(positionRechargementObusY.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du message "RECHARGEMENT..." de la roquette
                .key = cbor_move(cbor_build_string("|rechargementRoquetteX|")),
                .value = cbor_move(cbor_build_string(positionRechargementRoquetteX.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement du message "RECHARGEMENT..." de la roquette
                .key = cbor_move(cbor_build_string("|rechargementRoquetteY|")),
                .value = cbor_move(cbor_build_string(positionRechargementRoquetteY.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement de l'ennemi
                .key = cbor_move(cbor_build_string("|ennemiX|")),
                .value = cbor_move(cbor_build_string(positionEnnemiX.c_str()))
        });
        cbor_map_add(cbor_root, (struct cbor_pair) { // Déplacement de l'ennemi
                .key = cbor_move(cbor_build_string("|ennemiY|")),
                .value = cbor_move(cbor_build_string(positionEnnemiY.c_str()))
        });
        size_t buffer_size,
                        length = cbor_serialize_alloc(cbor_root, &buffer, &buffer_size);

        // Envoi du message
        if( (sendto(client, buffer, length, 0, (struct sockaddr*)&sin, to))< 0){
            printf(" error sendto\n");
            exit(1);
        }

        // Libération de la mémoire
        free(buffer);
        cbor_decref(&cbor_root);
      }
    }
}

// Nom de fonction : "deplacementTerre"
// Paramètres :
// Return : void
// Description : Cette fonction calcule la prochain déplacement de la terre, et
// en modifie sa position en fonction
void Client::deplacementTerre(){
  rotationTerre = to_string(stoi(rotationTerre) - 1);
  if(stoi(rotationTerre) == 0){
    rotationTerre = "360";
  }
  rotationTerreEntiere = "|rotate("+rotationTerre+" 379 439)|";
}

// Nom de fonction : "deplacementTankJoueur"
// Paramètres :
// Return : void
// Description : Cette fonction calcule la prochain déplacement du tank joueur, et
// en modifie sa position en fonction
void Client::deplacementTankJoueur(){
  if(decompteTankJoueur == 3){
    decompteTankJoueur = 0;
    if(tankJoueurActif == 1){
      positionTank1X = "|10000|";
      positionTank1Y = "|10000|";
      positionTank2X = "|121|";
      positionTank2Y = "|97|";
      if(!tirRoquetteEnCours){
        positionRoquetteXFormat = "121";
        positionRoquetteYFormat = "97";
        positionRoquetteX = "|"+positionRoquetteXFormat+"|";
        positionRoquetteY = "|"+positionRoquetteYFormat+"|";
      }
      positionArmementX = "|121|";
      positionArmementY = "|97|";
      tankJoueurActif = 2;
    }
    else{
      positionTank2X = "|10000|";
      positionTank2Y = "|10000|";
      positionTank1X = "|120|";
      positionTank1Y = "|98|";
      if(!tirRoquetteEnCours){
        positionRoquetteXFormat = "120";
        positionRoquetteYFormat = "98";
        positionRoquetteX = "|"+positionRoquetteXFormat+"|";
        positionRoquetteY = "|"+positionRoquetteYFormat+"|";
      }
      positionArmementX = "|120|";
      positionArmementY = "|98|";
      tankJoueurActif = 1;
    }
  }
  else{
    decompteTankJoueur++;
  }
}

// Nom de fonction : "tirerObus"
// Paramètres :
// Return : void
// Description : Cette fonction permet d'envoyer un signal indiquant qu'un obus
// a été tiré
void Client::tirerObus(){
  if(!tirObusEnCours){
    positionObusXFormat = "262";
    positionObusYFormat = "149";
    positionObusX = "|"+positionObusXFormat+"|";
    positionObusY = "|"+positionObusYFormat+"|";
    positionPretObusX = "|30000|";
    positionPretObusY = "|30000|";
    positionRechargementObusX = "|-182|";
    positionRechargementObusY = "|-290|";
    tirObusEnCours = true;
  }
}

// Nom de fonction : "tirerRoquette"
// Paramètres :
// Return : void
// Description : Cette fonction permet d'envoyer un signal indiquant qu'une roquette
// a été tiré
void Client::tirerRoquette(){
  if(!tirRoquetteEnCours){
    positionPretRoquetteX = "|30000|";
    positionPretRoquetteY = "|30000|";
    positionRechargementRoquetteX = "|-182|";
    positionRechargementRoquetteY = "|-312|";
    tirRoquetteEnCours = true;
  }
}

// Nom de fonction : "deplacementObus"
// Paramètres :
// Return : void
// Description : Cette fonction calcule la prochain déplacement de l'obus, et
// en modifie sa position en fonction
void Client::deplacementObus(){
  positionObusXFormat = to_string(stoi(positionObusXFormat) + 2);
  positionObusYFormat = to_string(stoi(positionObusYFormat) - 1);
  rechargementObus++;
  if(rechargementObus == 150){
    positionObusXFormat = "20000";
    positionObusYFormat = "20000";
    positionPretObusX = "|-210|";
    positionPretObusY = "|-285|";
    positionRechargementObusX = "|30000|";
    positionRechargementObusY = "|30000|";
    tirObusEnCours = false;
    rechargementObus = 0;
  }
  positionObusX = "|"+positionObusXFormat+"|";
  positionObusY = "|"+positionObusYFormat+"|";
}

// Nom de fonction : "deplacementRoquette"
// Paramètres :
// Return : void
// Description : Cette fonction calcule la prochain déplacement de la roquette, et
// en modifie sa position en fonction
void Client::deplacementRoquette(){
  positionRoquetteXFormat = to_string(stoi(positionRoquetteXFormat) + 3);
  positionRoquetteYFormat = to_string(stoi(positionRoquetteYFormat) - 5);
  rechargementRoquette++;
  if(rechargementRoquette == 100){
    if(tankJoueurActif == 1){
      positionRoquetteXFormat = "120";
      positionRoquetteYFormat = "98";
    }
    else{
      positionRoquetteXFormat = "121";
      positionRoquetteYFormat = "97";
    }
    positionPretRoquetteX = "|-210|";
    positionPretRoquetteY = "|-307|";
    positionRechargementRoquetteX = "|30000|";
    positionRechargementRoquetteY = "|30000|";
    tirRoquetteEnCours = false;
    rechargementRoquette = 0;
  }
  positionRoquetteX = "|"+positionRoquetteXFormat+"|";
  positionRoquetteY = "|"+positionRoquetteYFormat+"|";
}

// Nom de fonction : "creerEnnemi"
// Paramètres :
// Return : int
// Description : Cette fonction créé un ennemi en le positionnant à un endroit
// aléatoire sur la fenêtre. Cette fonction renvoie un entier correspondant à
// la position initiale de l'ennemi
int Client::creerEnnemi(){
  int ennemi = rand() % 4 + 1;
  switch(ennemi){
    // en haut à gauche
    case 1:
      positionEnnemiXFormat = "110";
      positionEnnemiYFormat = "-50";
      break;
    // en haut à droite
    case 2:
      positionEnnemiXFormat = "410";
      positionEnnemiYFormat = "-50";
      break;
    // en bas à gauche
    case 3:
      positionEnnemiXFormat = "110";
      positionEnnemiYFormat = "20";
      break;
    // en bas à droite
    case 4:
      positionEnnemiXFormat = "410";
      positionEnnemiYFormat = "20";
      break;
  }
  positionEnnemiX = "|"+positionEnnemiXFormat+"|";
  positionEnnemiY = "|"+positionEnnemiYFormat+"|";
  ennemiEnDeplacement = true;
  return ennemi;
}

// Nom de fonction : "deplacementEnnemi"
// Paramètres : int
// Return : void
// Description : Cette fonction calcule la prochain déplacement de l'ennemi, et
// en modifie sa position en fonction de sa position initiale
void Client::deplacementEnnemi(int typeEnnemi){
  nombreDeplacementEnnemi++;
  switch(typeEnnemi){
    // en haut à gauche
    case 1:
    positionEnnemiXFormat = to_string(stoi(positionEnnemiXFormat) + 2);
      break;
    // en haut à droite
    case 2:
    positionEnnemiXFormat = to_string(stoi(positionEnnemiXFormat) - 2);
      break;
    // en bas à gauche
    case 3:
    positionEnnemiXFormat = to_string(stoi(positionEnnemiXFormat) + 2);
      break;
    // en bas à droite
    case 4:
    positionEnnemiXFormat = to_string(stoi(positionEnnemiXFormat) -2);
      break;
  }
  positionEnnemiX = "|"+positionEnnemiXFormat+"|";
  if(nombreDeplacementEnnemi == 150){
    ennemiEnDeplacement = false;
    countdownEnnemi = 30;
    positionEnnemiXFormat = "50000";
    positionEnnemiX = "|"+positionEnnemiXFormat+"|";
    positionEnnemiYFormat = "50000";
    positionEnnemiY = "|"+positionEnnemiYFormat+"|";
    nombreDeplacementEnnemi = 0;
  }
}

// Nom de fonction : "lancement"
// Paramètres :
// Return : void
// Description : Cette fonction permet d'indiquer au client que le jeu est lançé
// et que la boucle principale peut être exécutée
void Client::lancement(){
  lancer = true;
}

// Le fichier Serveur.cpp récupère les messages cbor envoyés sur le port 6789,
// et modifie le fichier svg en fonction des informations récupérées.

// --includes--
#include "Serveur.h"

// --constructeur--
  Serveur::Serveur(){
    svg_data.LoadFile("ressources/toto.svg");
    svg_data.Print(&printer);
  }

  // --fonctions --

  // Nom de fonction : "start"
  // Paramètres : GtkWidget
  // Return : void
  // Description : Cette fonction créé le serveur qui récupère en continue les
  // messages envoyés sur le port 6789 venant de n'importe quelle source.
  // Elle appelle ensuite une fonction qui permet de modifier le document svg
  // en fonction des messages reçus
  void Serveur::start(GtkWidget *widget){

      // Création du serveur et de son adresse
        client = socket(AF_INET, SOCK_DGRAM, 0);
        if(client < 0){
             printf("erreur lors de la création de socket\n");
             exit(1);
        }
        printf("socket created\n");
        server_addr.sin_family = AF_INET;
        server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        server_addr.sin_port =htons(6789);
        if(bind(client, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0){
            printf("error bind socket\n");
            exit(1);
        }
        printf("waiting...\n");
        unsigned int size = sizeof(server_addr);

        // Boucle principale
        while(1){
          if(lancer){

            // Récuperation des messages
            if(( recvfrom (client, buffer, (sizeof(buffer) - 1), 0, (struct sockaddr*)&server_addr,  &size)) < 0){
                printf("pas de reception de message");
                exit(1);
            }

            // Parsing des messages reçus
            str_buffer = buffer;
            stringstream check1(str_buffer);
            while(getline(check1, intermediate, '|')){
              tokens.push_back(intermediate);
            }

            // Appel de la fonction de decryptage
            decrypteCbor(widget);

            // Libération de la mémoire
            tokens.clear();
        }
      }
  }

  // Nom de fonction : "decrypteCbor"
  // Paramètres : GtkWidget
  // Return : void
  // Description : Cette fonction modifie le fichier svg en fonction du message cbor
  // reçu précedemment, puis appelle la fonction de rafraichissement de la fenêtre
  void Serveur::decrypteCbor(GtkWidget *widget){
    svg_data.LoadFile("ressources/toto.svg");

    // Modification de la terre
    terre = svg_data.FirstChildElement("svg")->FirstChildElement("g");
    terre->SetAttribute("transform", tokens[3].c_str());

    // Modification du tank
    tank1 = svg_data.FirstChildElement("svg")->FirstChildElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg");
    tank1->SetAttribute("x", tokens[7].c_str());
    tank1->SetAttribute("y", tokens[11].c_str());

    // Modification du tank
    tank2 = svg_data.FirstChildElement("svg")->FirstChildElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg");
    tank2->SetAttribute("x", tokens[15].c_str());
    tank2->SetAttribute("y", tokens[19].c_str());

    // Modification de l'obus
    obus = svg_data.FirstChildElement("svg")->FirstChildElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg");
    obus->SetAttribute("x", tokens[23].c_str());
    obus->SetAttribute("y", tokens[27].c_str());

    // Modification de la roquette
    roquette = svg_data.FirstChildElement("svg")->FirstChildElement("svg");
    roquette->SetAttribute("x", tokens[31].c_str());
    roquette->SetAttribute("y", tokens[35].c_str());

    // Modification de l'armement
    armement = svg_data.FirstChildElement("svg")->FirstChildElement("svg")->NextSiblingElement("svg");
    armement->SetAttribute("x", tokens[39].c_str());
    armement->SetAttribute("y", tokens[43].c_str());

    // Modification du message "PRÊT !" de l'obus
    pretObus = svg_data.FirstChildElement("svg")->FirstChildElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg");
    pretObus->SetAttribute("x", tokens[55].c_str());
    pretObus->SetAttribute("y", tokens[59].c_str());

    // Modification du message "PRÊT !" de la roquette
    pretRoquette = svg_data.FirstChildElement("svg")->FirstChildElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg");
    pretRoquette->SetAttribute("x", tokens[47].c_str());
    pretRoquette->SetAttribute("y", tokens[51].c_str());

    // Modification du message "RECHARGEMENT..." de l'obus
    rechargeObus = svg_data.FirstChildElement("svg")->FirstChildElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg");
    rechargeObus->SetAttribute("x", tokens[63].c_str());
    rechargeObus->SetAttribute("y", tokens[67].c_str());

    // Modification du message "RECHARGEMENT..." de la roquette
    rechargeRoquette = svg_data.FirstChildElement("svg")->FirstChildElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg");
    rechargeRoquette->SetAttribute("x", tokens[71].c_str());
    rechargeRoquette->SetAttribute("y", tokens[75].c_str());

    // Modification de l'ennemi
    ennemi = svg_data.FirstChildElement("svg")->FirstChildElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg")->NextSiblingElement("svg");
    ennemi->SetAttribute("x", tokens[79].c_str());
    ennemi->SetAttribute("y", tokens[83].c_str());

    // Rafraichissement de la fenêtre
    svg_data.SaveFile("ressources/toto.svg");
    gtk_widget_queue_draw(widget);

  }

  // Nom de fonction : "lancement"
  // Paramètres :
  // Return : void
  // Description : Cette fonction permet d'indiquer au serveur que le jeu est lançé
  // et que la boucle principale peut être exécutée
  void Serveur::lancement(){
    lancer = true;
  }

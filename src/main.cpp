// Le fichier main.cpp permet de lancer le programme. il lance les 3 threads principaux
// correspondants à la boucle gtk_main(), au client et au serveur. Par ailleurs, il gère
// aussi la détection du clavier lorsque l'on appuye sur une touche.

// --includes--
#include <cairo.h>
#include <gtk/gtk.h>
#include <librsvg/rsvg.h>
#include <gio/gfile.h>
#include <tinyxml2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <thread>
#include <iostream>
#include <chrono>
#include "Serveur.h"
#include "Client.h"

// --packages--
using namespace tinyxml2;
using namespace std;

// --variables--
static void do_drawing(cairo_t *);
static void do_drawing_svg(cairo_t *);
Serveur *serveur = new Serveur();
Client *client = new Client();
XMLDocument svg_data;
XMLElement *terre;
XMLPrinter printer;
GtkWidget *window;
GtkWidget *darea;
string rotationTerre = "360";
string rotationTerreEntiere = "rotate("+rotationTerre+" 379 439)";
RsvgHandle *svg_handle;
bool jeuLance = false;

// --fonctions--

// Nom de fonction : "on_draw_event"
// Paramètres : GtkWidget*, cairo_t*, gpointer
// Return : gboolean
// Description : Cette fonction récupère les éléments composant la fenêtre affichée
// et appelle une fonction qui dessine sur cette fenêtre
static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr,
    gpointer user_data)
{
  do_drawing_svg(cr);

  return FALSE;
}

// Nom de fonction : "do_drawing_svg"
// Paramètres : cairo_t*
// Return : void
// Description : Cette fonction dessine sur la fenêtre principale en fonction du
// document svg entré en paramètre.
static void do_drawing_svg(cairo_t * cr)
{
  if(jeuLance){
    // si le jeu est lançé, on charge le document "toto.svg"
    svg_data.LoadFile("ressources/toto.svg");
    printer.ClearBuffer();
    svg_data.Print(&printer);
    svg_handle = rsvg_handle_new_from_data ((const unsigned char*) printer.CStr(), printer.CStrSize()-1, NULL);
    rsvg_handle_render_cairo(svg_handle, cr);
  }
  else{
    // si le jeu n'est pas lançé, on charge le document "Menu.svg"
    svg_data.LoadFile("ressources/Menu.svg");
    printer.ClearBuffer();
    svg_data.Print(&printer);
    svg_handle = rsvg_handle_new_from_data ((const unsigned char*) printer.CStr(), printer.CStrSize()-1, NULL);
    rsvg_handle_render_cairo(svg_handle, cr);
  }
}

// Nom de fonction : "lancement_serveur"
// Paramètres :
// Return : void
// Description : Cette fonction lance le serveur en envoyant la zone de dessin
// afin de pouvoir la mettre à jour
static void lancement_serveur(){
  serveur->start(darea);
}

// Nom de fonction : "lancement_client_boucle_principale"
// Paramètres :
// Return : void
// Description : Cette fonction lance le client et la boucle principale de l'animation
static void lancement_client_boucle_principale(){
  client->bouclePrincipale();
}

// Nom de fonction : "my_keypress_function"
// Paramètres : GtkWidget*, GdkEventKey*, gpointer, cairo_t*
// Return : gboolean
// Description : Cette fonction répond à l'évenement suivant : si une touche du
// clavier est enfoncée. Certaines actions sont lancées en fonction de la touche appuyée
static gboolean my_keypress_function (GtkWidget *widget, GdkEventKey *event, gpointer data, cairo_t *cr) {
  if(jeuLance){
    if (event->keyval == GDK_KEY_s){
      // Touche "S" appuyée
        client->tirerRoquette();
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_d){
      // Touche "D" appuyée
        client->tirerObus();
        return TRUE;
    }
  }
  else{
    if (event->keyval == GDK_KEY_Return){
      // Touche "Entrée" appuyée
        client->lancement();
        serveur->lancement();
        jeuLance = true;
        return TRUE;
    }
  }
    return FALSE;
}

// Nom de fonction : "main"
// Paramètres : int, char*
// Return : int
// Description : Cette fonction se lance au début du programme et initialise la
// fenêtre qui s'affichera en créant les évenements de cette dernière. Elle
// créé aussi les threads, et les exécutes ensuite
int main(int argc, char *argv[])
{
  // Initialisation de la fenêtre
  gtk_init(&argc, &argv);
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  darea = gtk_drawing_area_new();
  gtk_container_add(GTK_CONTAINER(window), darea);
  svg_data.LoadFile("ressources/initialisation.svg");
  svg_data.SaveFile("ressources/toto.svg");
  svg_data.Print(&printer);
  svg_handle = rsvg_handle_new_from_data ((const unsigned char*) printer.CStr(), printer.CStrSize()-1, NULL);
  terre = svg_data.FirstChildElement("svg");

  // Création des évenements
  gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
  g_signal_connect (G_OBJECT (window), "key_press_event", G_CALLBACK (my_keypress_function), NULL);
  g_signal_connect(G_OBJECT(darea), "draw",
      G_CALLBACK(on_draw_event), NULL);
  g_signal_connect(window, "destroy",
      G_CALLBACK(gtk_main_quit), NULL);

// Positionnement et taille de la fenêtre
  gtk_window_set_default_size(GTK_WINDOW(window), 850, 850);
  gtk_window_set_resizable (GTK_WINDOW(window), FALSE);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_window_set_title(GTK_WINDOW(window), "GTK window");
  gtk_widget_show_all(window);

// Créations des threads
  thread premier (gtk_main); // Affichage de la fenêtre
  thread deuxieme (lancement_serveur); // Serveur
  thread troisieme (lancement_client_boucle_principale); // Client

// Execution des threads
  premier.join();
  deuxieme.join();
  troisieme.join();

  return 0;
}

Exmplications des fichierSVG:


- arme_sol: image du projectile du personnage pour attaquer les ennemis au sol

- arme_vol: image du projectile du personnage pour attaquer les ennemis en l'air

- armement: image de la partie haute du personnage. Cette partie ne bouge pas

- armement_ennemi: image de la partie haute de l'ennemi au sol. Cette partie ne bouge pas

- Lune: image de la lune

- Perso: image de l'ancien personnage

- soleil: image du soleil

- tank en: image complète de l'ennemi au sol

- Tank1: première image de la partie basse du personnage pour le deplacement

- tank2: deuxième image de la partie basse du personnage pour le deplacement

- toto: image de la planète qui tourne sur elle même

- Vol_ennemi: image de l'ennemi en l'air
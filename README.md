# Goodbody

Application graphique utilisant la librairie gtk et mise à jour avec cairo utilisant un fichier svg.
Envoi de données via un système client-serveur configurés en UDP sur le port 6789.
Application programmée en C++ et xml.
Application compilée via un Makefile utilisant gcc.
Librairies à installer : 
	-cairo 
	-gtk+-3.0 
	-librsvg-2.0 
	-tinyxml2 
	-libcbor
Application sauvegardé sur GitLab : https://gitlab.com/Legirasa/goodbody

Collaborateurs :
	- RAOUL Steven
	- PERREIN Adrien
	- STEDE-SCHRADER Anthony

Instructions de compilation : 

Commande à entrer dans le terminal : make

Instructions de lancement de programme :

Commande à entrer dans le terminal : ./main


ATTENTION ! 
Le programme utilise un fichier svg très lourd. 
Tous les ordinateurs ne sont pas capables de rafraichir la fenêtre Gtk assez rapidement,
et le programme peut crash à cause de cela.

Programme testé avec la configuration suivante : 
	- Processeur i7 4790
	- 8 Go RAM
	- OS Xubuntu 11.04
